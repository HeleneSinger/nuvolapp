pipeline {
    agent any
    environment {
        MONGO_HOST = credentials('mongo_db_host')
        DOCKER_HUB_PASSWORD = credentials('docker_hub_password')
        STAGING_IDADDRESS = "3.123.33.78"
    }
    stages {
        stage('Analyse de code') {
            steps {
                sh 'docker run --rm -v /var/lib/docker/volumes/jenkins-data/_data/workspace/nuvolapp:/src opensecurity/njsscan /src'
            }
        }
        stage('Linter') {
            steps {
                sh 'docker run --rm -v /var/lib/docker/volumes/jenkins-data/_data/workspace:/data cytopia/eslint .'
            }
        }
        stage('build') {
            steps {
                sh 'docker build -t vaylle/nuvolapp:${BUILD_NUMBER} .'
            }
        }
        stage('test') {
            steps {
                sh 'docker rm -f nuvolapp'
                sh 'docker run -d --name nuvolapp --net jenkins --env MONGO_HOST=${MONGO_HOST} vaylle/nuvolapp:${BUILD_NUMBER}'
                sh 'sleep 10'
                sh 'curl -Is nuvolapp:4000 | head -n 1 | grep "OK"'
                sh 'docker rm -f nuvolapp'
            }
        }
        stage('Publish image') {
            steps {
                sh 'echo -n ${DOCKER_HUB_PASSWORD} | docker login -u vaylle --password-stdin'
                sh 'docker push vaylle/nuvolapp:${BUILD_NUMBER}'
            }
        }

        stage('Staging') {
            steps {
                withCredentials([sshUserPrivateKey(credentialsId: 'staging_ssh_key', keyFileVariable: 'SSH_KEY')]) {
                    //Staging dans le cloud aws sur une machine existante

                    sh 'ssh -o StrictHostKeyChecking=no ubuntu@${STAGING_IDADDRESS} -i ${SSH_KEY} "sudo docker pull vaylle/nuvolapp:${BUILD_NUMBER}" '
                    sh 'ssh -o StrictHostKeyChecking=no ubuntu@${STAGING_IDADDRESS} -i ${SSH_KEY} "sudo docker rm -f nuvolapp" '
                    sh 'ssh -o StrictHostKeyChecking=no ubuntu@${STAGING_IDADDRESS} -i ${SSH_KEY} "sudo docker run -d --name nuvolapp -p 4000:80 --env MONGO_HOST=\"${MONGO_HOST}\" vaylle/nuvolapp:${BUILD_NUMBER}" '
                }
            }
        }
    }
    post { 
        success { 
            sh 'curl -Is ${STAGING_IDADDRESS} | head -n 1 | grep OK'
        }
    }
}
